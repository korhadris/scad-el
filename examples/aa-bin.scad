$fn = 30;

difference() {
  cube([50.000, 120.000, 20.000]);

  // Rows: 8  Cols: 3
  // Row-gap: 0.000000  Col-gap: 1.250000
  translate([8.750, 7.500, 0.840]) {
    cylinder(h = 20.000, r1 = 7.500, r2 = 7.500);
  }  // translate

  translate([25.000, 7.500, 0.840]) {
    cylinder(h = 20.000, r1 = 7.500, r2 = 7.500);
  }  // translate

  translate([41.250, 7.500, 0.840]) {
    cylinder(h = 20.000, r1 = 7.500, r2 = 7.500);
  }  // translate

  translate([8.750, 22.500, 0.840]) {
    cylinder(h = 20.000, r1 = 7.500, r2 = 7.500);
  }  // translate

  translate([25.000, 22.500, 0.840]) {
    cylinder(h = 20.000, r1 = 7.500, r2 = 7.500);
  }  // translate

  translate([41.250, 22.500, 0.840]) {
    cylinder(h = 20.000, r1 = 7.500, r2 = 7.500);
  }  // translate

  translate([8.750, 37.500, 0.840]) {
    cylinder(h = 20.000, r1 = 7.500, r2 = 7.500);
  }  // translate

  translate([25.000, 37.500, 0.840]) {
    cylinder(h = 20.000, r1 = 7.500, r2 = 7.500);
  }  // translate

  translate([41.250, 37.500, 0.840]) {
    cylinder(h = 20.000, r1 = 7.500, r2 = 7.500);
  }  // translate

  translate([8.750, 52.500, 0.840]) {
    cylinder(h = 20.000, r1 = 7.500, r2 = 7.500);
  }  // translate

  translate([25.000, 52.500, 0.840]) {
    cylinder(h = 20.000, r1 = 7.500, r2 = 7.500);
  }  // translate

  translate([41.250, 52.500, 0.840]) {
    cylinder(h = 20.000, r1 = 7.500, r2 = 7.500);
  }  // translate

  translate([8.750, 67.500, 0.840]) {
    cylinder(h = 20.000, r1 = 7.500, r2 = 7.500);
  }  // translate

  translate([25.000, 67.500, 0.840]) {
    cylinder(h = 20.000, r1 = 7.500, r2 = 7.500);
  }  // translate

  translate([41.250, 67.500, 0.840]) {
    cylinder(h = 20.000, r1 = 7.500, r2 = 7.500);
  }  // translate

  translate([8.750, 82.500, 0.840]) {
    cylinder(h = 20.000, r1 = 7.500, r2 = 7.500);
  }  // translate

  translate([25.000, 82.500, 0.840]) {
    cylinder(h = 20.000, r1 = 7.500, r2 = 7.500);
  }  // translate

  translate([41.250, 82.500, 0.840]) {
    cylinder(h = 20.000, r1 = 7.500, r2 = 7.500);
  }  // translate

  translate([8.750, 97.500, 0.840]) {
    cylinder(h = 20.000, r1 = 7.500, r2 = 7.500);
  }  // translate

  translate([25.000, 97.500, 0.840]) {
    cylinder(h = 20.000, r1 = 7.500, r2 = 7.500);
  }  // translate

  translate([41.250, 97.500, 0.840]) {
    cylinder(h = 20.000, r1 = 7.500, r2 = 7.500);
  }  // translate

  translate([8.750, 112.500, 0.840]) {
    cylinder(h = 20.000, r1 = 7.500, r2 = 7.500);
  }  // translate

  translate([25.000, 112.500, 0.840]) {
    cylinder(h = 20.000, r1 = 7.500, r2 = 7.500);
  }  // translate

  translate([41.250, 112.500, 0.840]) {
    cylinder(h = 20.000, r1 = 7.500, r2 = 7.500);
  }  // translate
}  // difference
