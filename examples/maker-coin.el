(require 'scad)

(scad-let
    ((width 30.0)
     (height 51.0)
     (thickness 2.6)
     (edge-radius (* 0.45 thickness))
     (hole-radius 1.6)
     (layer-height 0.2)
     (nozzle-width 0.4)
     (bottom-layers 2)
     (top-layers 3)
     (poly-lines 2)
     (bottom-thickness (* layer-height bottom-layers))
     (void-thickness (- thickness bottom-thickness (* layer-height top-layers)))
     (delta-x (- (* 0.5 width) edge-radius))
     (delta-y (- (* 0.25 width) edge-radius))
     (half-side (* 0.5 (- height (* 0.5 width))))
     (inner-poly-size (/ width 10))
     (poly-offset (/ width 4.5)))
  (scad-write-file "maker-coin.scad"
    (scad-set-variable "$fn" 8)
    (scad-difference
        (scad-translate 0 0 edge-radius
          (scad-minkowski
          (scad-sphere edge-radius)
          (scad-linear-extrude ((max fudge (- thickness (* 2 edge-radius))))
            (scad-polygon (list
                           (list delta-x (- half-side))
                           (list delta-x half-side)
                           (list 0 (+ half-side delta-y))
                           (list (- delta-x) half-side)
                           (list (- delta-x) (- half-side))
                           (list 0 (- (- half-side) delta-y))))
            ;; (scad-regular-polygon 6 (- (* 0.5 width) edge-radius) :inner :rotated)
            )))
      (scad-translate 0 (- (* 0.5 height) edge-radius (* 2 hole-radius)) -fudge
        (scad-cylinder (+ (* 2 fudge) thickness) hole-radius nil nil nil nil 30))
      (scad-translate 0 (- (- (* 0.5 height) edge-radius (* 2 hole-radius))) -fudge
        (scad-cylinder (+ (* 2 fudge) thickness) hole-radius nil nil nil nil 30))
      (scad-translate 0 0 bottom-thickness
        (scad-linear-extrude (void-thickness)
          (scad-regular-polygon 6 inner-poly-size :inner))
        (cl-loop for angle from 30 below 360 by 60
                 concat (scad-translate
                            (* poly-offset (cos (degrees-to-radians angle)))
                            (* poly-offset (sin (degrees-to-radians angle)))
                            0
                          (scad-difference
                              (scad-linear-extrude (void-thickness)
                                (scad-regular-polygon 6 inner-poly-size :inner))
                            (scad-linear-extrude (void-thickness)
                              (scad-regular-polygon 6 (- inner-poly-size (* poly-lines nozzle-width)))))))
        (scad-difference
            (scad-cylinder void-thickness (* 4 inner-poly-size) nil nil nil nil 30)
          (scad-cylinder void-thickness (- (* 4 inner-poly-size) 0.45) nil nil nil nil 30)
          )))))
