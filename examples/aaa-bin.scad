$fn = 30;

difference() {
  cube([50.000, 120.000, 20.000]);

  // Rows: 11  Cols: 4
  // Row-gap: 0.008333  Col-gap: 1.280000
  translate([6.730, 5.458, 0.840]) {
    cylinder(h = 20.000, r1 = 5.450, r2 = 5.450);
  }  // translate

  translate([18.910, 5.458, 0.840]) {
    cylinder(h = 20.000, r1 = 5.450, r2 = 5.450);
  }  // translate

  translate([31.090, 5.458, 0.840]) {
    cylinder(h = 20.000, r1 = 5.450, r2 = 5.450);
  }  // translate

  translate([43.270, 5.458, 0.840]) {
    cylinder(h = 20.000, r1 = 5.450, r2 = 5.450);
  }  // translate

  translate([6.730, 16.367, 0.840]) {
    cylinder(h = 20.000, r1 = 5.450, r2 = 5.450);
  }  // translate

  translate([18.910, 16.367, 0.840]) {
    cylinder(h = 20.000, r1 = 5.450, r2 = 5.450);
  }  // translate

  translate([31.090, 16.367, 0.840]) {
    cylinder(h = 20.000, r1 = 5.450, r2 = 5.450);
  }  // translate

  translate([43.270, 16.367, 0.840]) {
    cylinder(h = 20.000, r1 = 5.450, r2 = 5.450);
  }  // translate

  translate([6.730, 27.275, 0.840]) {
    cylinder(h = 20.000, r1 = 5.450, r2 = 5.450);
  }  // translate

  translate([18.910, 27.275, 0.840]) {
    cylinder(h = 20.000, r1 = 5.450, r2 = 5.450);
  }  // translate

  translate([31.090, 27.275, 0.840]) {
    cylinder(h = 20.000, r1 = 5.450, r2 = 5.450);
  }  // translate

  translate([43.270, 27.275, 0.840]) {
    cylinder(h = 20.000, r1 = 5.450, r2 = 5.450);
  }  // translate

  translate([6.730, 38.183, 0.840]) {
    cylinder(h = 20.000, r1 = 5.450, r2 = 5.450);
  }  // translate

  translate([18.910, 38.183, 0.840]) {
    cylinder(h = 20.000, r1 = 5.450, r2 = 5.450);
  }  // translate

  translate([31.090, 38.183, 0.840]) {
    cylinder(h = 20.000, r1 = 5.450, r2 = 5.450);
  }  // translate

  translate([43.270, 38.183, 0.840]) {
    cylinder(h = 20.000, r1 = 5.450, r2 = 5.450);
  }  // translate

  translate([6.730, 49.092, 0.840]) {
    cylinder(h = 20.000, r1 = 5.450, r2 = 5.450);
  }  // translate

  translate([18.910, 49.092, 0.840]) {
    cylinder(h = 20.000, r1 = 5.450, r2 = 5.450);
  }  // translate

  translate([31.090, 49.092, 0.840]) {
    cylinder(h = 20.000, r1 = 5.450, r2 = 5.450);
  }  // translate

  translate([43.270, 49.092, 0.840]) {
    cylinder(h = 20.000, r1 = 5.450, r2 = 5.450);
  }  // translate

  translate([6.730, 60.000, 0.840]) {
    cylinder(h = 20.000, r1 = 5.450, r2 = 5.450);
  }  // translate

  translate([18.910, 60.000, 0.840]) {
    cylinder(h = 20.000, r1 = 5.450, r2 = 5.450);
  }  // translate

  translate([31.090, 60.000, 0.840]) {
    cylinder(h = 20.000, r1 = 5.450, r2 = 5.450);
  }  // translate

  translate([43.270, 60.000, 0.840]) {
    cylinder(h = 20.000, r1 = 5.450, r2 = 5.450);
  }  // translate

  translate([6.730, 70.908, 0.840]) {
    cylinder(h = 20.000, r1 = 5.450, r2 = 5.450);
  }  // translate

  translate([18.910, 70.908, 0.840]) {
    cylinder(h = 20.000, r1 = 5.450, r2 = 5.450);
  }  // translate

  translate([31.090, 70.908, 0.840]) {
    cylinder(h = 20.000, r1 = 5.450, r2 = 5.450);
  }  // translate

  translate([43.270, 70.908, 0.840]) {
    cylinder(h = 20.000, r1 = 5.450, r2 = 5.450);
  }  // translate

  translate([6.730, 81.817, 0.840]) {
    cylinder(h = 20.000, r1 = 5.450, r2 = 5.450);
  }  // translate

  translate([18.910, 81.817, 0.840]) {
    cylinder(h = 20.000, r1 = 5.450, r2 = 5.450);
  }  // translate

  translate([31.090, 81.817, 0.840]) {
    cylinder(h = 20.000, r1 = 5.450, r2 = 5.450);
  }  // translate

  translate([43.270, 81.817, 0.840]) {
    cylinder(h = 20.000, r1 = 5.450, r2 = 5.450);
  }  // translate

  translate([6.730, 92.725, 0.840]) {
    cylinder(h = 20.000, r1 = 5.450, r2 = 5.450);
  }  // translate

  translate([18.910, 92.725, 0.840]) {
    cylinder(h = 20.000, r1 = 5.450, r2 = 5.450);
  }  // translate

  translate([31.090, 92.725, 0.840]) {
    cylinder(h = 20.000, r1 = 5.450, r2 = 5.450);
  }  // translate

  translate([43.270, 92.725, 0.840]) {
    cylinder(h = 20.000, r1 = 5.450, r2 = 5.450);
  }  // translate

  translate([6.730, 103.633, 0.840]) {
    cylinder(h = 20.000, r1 = 5.450, r2 = 5.450);
  }  // translate

  translate([18.910, 103.633, 0.840]) {
    cylinder(h = 20.000, r1 = 5.450, r2 = 5.450);
  }  // translate

  translate([31.090, 103.633, 0.840]) {
    cylinder(h = 20.000, r1 = 5.450, r2 = 5.450);
  }  // translate

  translate([43.270, 103.633, 0.840]) {
    cylinder(h = 20.000, r1 = 5.450, r2 = 5.450);
  }  // translate

  translate([6.730, 114.542, 0.840]) {
    cylinder(h = 20.000, r1 = 5.450, r2 = 5.450);
  }  // translate

  translate([18.910, 114.542, 0.840]) {
    cylinder(h = 20.000, r1 = 5.450, r2 = 5.450);
  }  // translate

  translate([31.090, 114.542, 0.840]) {
    cylinder(h = 20.000, r1 = 5.450, r2 = 5.450);
  }  // translate

  translate([43.270, 114.542, 0.840]) {
    cylinder(h = 20.000, r1 = 5.450, r2 = 5.450);
  }  // translate
}  // difference
