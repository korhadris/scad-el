$fn = 30;

difference() {
  sphere(20.000);

  cylinder(h = 40.000, r1 = 10.000, r2 = 10.000, center = true);

  rotate(a = 90.000, v = [1.000, 0.000, 0.000]) {
    cylinder(h = 40.000, r1 = 10.000, r2 = 10.000, center = true);
  }  // rotate

  rotate(a = 90.000, v = [0.000, 1.000, 0.000]) {
    cylinder(h = 40.000, r1 = 10.000, r2 = 10.000, center = true);
  }  // rotate
}  // difference
