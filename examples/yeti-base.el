(require 'scad)

(scad-let
    ((yeti-diameter 84.0)
     (bottom-thickness 3.2)
     (wall-thickness 2.5)
     (wall-height (- 50.8 bottom-thickness))
     (height (+ bottom-thickness wall-height))
     (yeti-radius (* 0.5 yeti-diameter))
     (hex-radius (+ wall-thickness yeti-radius)))
  (scad-write-file "yeti-base.scad"
    (scad-set-variable "$fn" 60)
    (scad-difference
        (scad-linear-extrude (height)
          (scad-regular-polygon 6 hex-radius :inner))
      (scad-translate 0 0 bottom-thickness
        (scad-cylinder height yeti-radius))
      (scad-difference
          (scad-cylinder (+ height fudge) yeti-diameter)
        (scad-cylinder height (+ (* 2 wall-thickness) yeti-radius)))
      (scad-difference
          (scad-cylinder (* 0.5 wall-thickness) (+ hex-radius (* 2 wall-thickness)))
        (scad-translate 0 0 (* 0.5 wall-thickness)
               (scad-rotate (180 '(1 0 0))
                 (scad-cone hex-radius (+ hex-radius wall-thickness)))))
      (scad-translate 0 0 (- height (* 0.5 wall-thickness))
        (scad-difference
            (scad-cylinder (* 0.5 wall-thickness) (+ hex-radius (* 2 wall-thickness)))
          (scad-cone hex-radius (+ hex-radius wall-thickness)))))))
