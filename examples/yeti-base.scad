$fn = 60;

difference() {
  linear_extrude(height = 50.800) {
    polygon([[51.384, 0.000], [25.692, 44.500], [-25.692, 44.500], [-51.384, 0.000], [-25.692, -44.500], [25.692, -44.500]]);
  }  // linear_extrude

  translate([0.000, 0.000, 3.200]) {
    cylinder(h = 50.800, r1 = 42.000, r2 = 42.000);
  }  // translate

  difference() {
    cylinder(h = 50.801, r1 = 84.000, r2 = 84.000);

    cylinder(h = 50.800, r1 = 47.000, r2 = 47.000);
  }  // difference

  difference() {
    cylinder(h = 1.250, r1 = 49.500, r2 = 49.500);

    translate([0.000, 0.000, 1.250]) {
      rotate(a = 180.000, v = [1.000, 0.000, 0.000]) {
        cylinder(h = 44.500, r1 = 47.000, r2 = 0.000);
      }  // rotate
    }  // translate
  }  // difference

  translate([0.000, 0.000, 49.550]) {
    difference() {
      cylinder(h = 1.250, r1 = 49.500, r2 = 49.500);

      cylinder(h = 44.500, r1 = 47.000, r2 = 0.000);
    }  // difference
  }  // translate
}  // difference
