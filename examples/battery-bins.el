(require 'scad)

(scad-let
    ((bin-width 50.0)
     (bin-depth 120.0)
     (bin-height 20.0)
     (bin-floor (* 3 0.28))
     (min-thickness 0.0)
     (diameter-tolerance 0.4)
     (aa-diameter (+ 14.6 diameter-tolerance))
     (aaa-diameter (+ 10.5 diameter-tolerance))
     (battery-holder
      (bin-width bin-depth bin-height bin-floor min-thickness battery-diameter)
      (scad-let
          ((battery-radius (* 0.5 battery-diameter))
           (num-columns (floor (/ (- bin-width min-thickness)
                                  (+ battery-diameter min-thickness))))
           (num-rows (floor (/ (- bin-depth min-thickness)
                               (+ battery-diameter min-thickness))))
           (col-gap (/ (- bin-width (* battery-diameter num-columns))
                       (1+ num-columns)))
           (row-gap (/ (- bin-depth (* battery-diameter num-rows))
                       (1+ num-rows))))
        (concat
         (scad-set-variable "$fn" 30)
         (scad-difference
             (scad-cube bin-width bin-depth bin-height)
           (scad-comment (format "Rows: %d  Cols: %d" num-rows num-columns))
           (scad-comment (format "Row-gap: %f  Col-gap: %f" row-gap col-gap))
           (cl-loop
            for row below num-rows
            concat (cl-loop
                    for col below num-columns
                    concat (scad-translate
                               (+ col-gap battery-radius (* col (+ col-gap battery-diameter)))
                               (+ row-gap battery-radius (* row (+ row-gap battery-diameter)))
                               bin-floor
                             (scad-cylinder bin-height battery-radius)))))))))
  (scad-write-file "aa-bin.scad"
    (battery-holder bin-width bin-depth bin-height bin-floor min-thickness aa-diameter))
  (scad-write-file "aaa-bin.scad"
    (battery-holder bin-width bin-depth bin-height bin-floor min-thickness aaa-diameter)))
