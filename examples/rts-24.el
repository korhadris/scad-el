(require 'scad)

(scad-let
 ((height 2.4)
  (points 24)
  (inner-radius 16.5)
  (outer-radius 17.5)
  (layer-height 0.2)
  (bottom-layers 2)
  (top-layers 2)
  (void-offset (* bottom-layers layer-height))
  (void-thickness (- height void-offset (* top-layers layer-height)))
  (rts (width length height)
       (scad-translate (* -0.5 width) (* -0.5 length) 0
         (scad-linear-extrude (height)
           (scad-scale (/ width 120.0) (/ length 120.0) 1.0
             ;; R
             (scad-polygon
              '((3 56) (3 94) (5 97) (8 100) (48 100) (51 98) (53 95) (53 65) (48 60) (31 60)
                (53 25) (53 20) (47 20) (23 60) (23 67) (45 67) (46 68) (46 93) (10 93) (10 56)))
             ;; T
             (scad-polygon
              '((3 104) (7 111) (11 115) (21 120) (102 120) (112 115) (116 111) (120 104) (113 105)
                (107 109) (102 113) (65 113) (65 20) (64 19) (59 19) (58 20) (58 113) (21 113)
                (16 109) (10 105)))
             ;; S
             (scad-polygon
              '((0 32) (0 35) (5 50) (8 50) (13 35) (13 32) (10 32) (10 21) (15 16) (21 11)
                (102 11) (108 15) (113 21) (113 59) (112 60) (74 60) (70 65) (70 95) (74 100)
                (114 100) (120 95) (120 86) (113 86) (113 93) (76 93) (76 67) (115 67) (120 61)
                (120 20) (113 12) (110 9) (103 4) (20 4) (13 9) (10 12) (3 20) (3 32))))))))
 (scad-write-file "rts-24.scad"
   (scad-set-variable "$fn" 30)
   (scad-comment "Main token")
   (scad-difference
       (scad-union
        (scad-linear-extrude ((* 0.2 height) nil nil nil nil (/ 0.98))
          (scad-scale 0.98 0.98 1 (scad-star points inner-radius outer-radius)))
        (scad-translate 0 0 (* 0.2 height)
          (scad-linear-extrude ((* 0.8 height) nil nil (/ -360 (* 2 points)) nil 0.9 )
            (scad-star points inner-radius outer-radius))))
     (scad-comment "Badge Slot")
     (scad-translate 0 12 -fudge
       (scad-linear-extrude ((+ height (* 2 fudge)))
         (scad-hull
          (scad-translate -3 0 0 (scad-circle 1.2))
          (scad-translate 3 0 0 (scad-circle 1.2)))))
     (scad-comment "RTS Logo")
     (scad-translate 0 4 void-offset (rts 18 12 void-thickness))
     (scad-comment "Year")
     (scad-translate 0.3 -11 void-offset
       (scad-linear-extrude (void-thickness)
         (scad-scale 1.3 0.8 1 (scad-text "24" nil "Impact" "center")))))))
