(require 'scad)

(scad-let
    ((sphere-radius 20.0)
     (cylinder-radius 10.0)
     (cylinder-height (* 2 sphere-radius)))
  (scad-write-file "scad-el-example.scad"
    (scad-set-variable "$fn" 30)
    (scad-difference
        (scad-sphere sphere-radius)
      (scad-cylinder cylinder-height cylinder-radius 'nil :CENTER)
      (scad-rotate (90 '(1 0 0))
        (scad-cylinder cylinder-height cylinder-radius 'nil :CENTER))
      (scad-rotate (90 '(0 1 0))
        (scad-cylinder cylinder-height cylinder-radius 'nil :CENTER)))))
