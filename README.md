# scad-el

Emacs Lisp script to generate [OpenSCAD](https://openscad.org)
scripts.

## Setup instructions

Copy `scad.el` to a directory in your `load-path`, or add the
directory containing `scad.el` to your `load-path`:

```lisp
(add-to-lisp 'load-path "/path/to/scad-el")
```

## Short example [scad-el-example.el](examples/sacd-el-example.el)

```lisp
(require 'scad)

(scad-let
    ((sphere-radius 20.0)
     (cylinder-radius 10.0)
     (cylinder-height (* 2 sphere-radius)))
  (scad-write-file "scad-el-example.scad"
    (scad-set-variable "$fn" 30)
    (scad-difference
        (scad-sphere sphere-radius)
      (scad-cylinder cylinder-height cylinder-radius 'nil :CENTER)
      (scad-rotate (90 '(1 0 0))
        (scad-cylinder cylinder-height cylinder-radius 'nil :CENTER))
      (scad-rotate (90 '(0 1 0))
        (scad-cylinder cylinder-height cylinder-radius 'nil :CENTER)))))
```

Will write out [scad-el-example.scad](examples/scad-el-example.scad):
```
$fn = 30;

difference() {
  sphere(20.000);

  cylinder(h = 40.000, r1 = 10.000, r2 = 10.000, center = true);

  rotate(a = 90.000, v = [1.000, 0.000, 0.000]) {
    cylinder(h = 40.000, r1 = 10.000, r2 = 10.000, center = true);
  }  // rotate

  rotate(a = 90.000, v = [0.000, 1.000, 0.000]) {
    cylinder(h = 40.000, r1 = 10.000, r2 = 10.000, center = true);
  }  // rotate
}  // difference
```

![Example image](examples/scad-el-example.png)


## Background
I was unsatisfied with some aspects of the OpenSCAD scripting language
(primarily dealing with loops), and I wanted something more powerful,
and turned to Lisp. The first implementation was done in Common Lisp,
but I decided that writing something in Emacs, to then use CL to
convert was just an extra step that Emacs could do directly.

Example Emacs Lisp files with the OpenSCAD scripts generated from them
are included in the [examples](examples/) directory.
