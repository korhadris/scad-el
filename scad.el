;; -*- lexical-binding: t; -*-
(require 'cl-lib)

(defcustom scad-indent-str "  "
  "Indentation for `scad-convert'")

(defcustom scad-max-precision 3
  "Max number of decimal places in SCAD output")

(defcustom scad-fudge 0.001
  "Fudge factor used to ensure overlap of some objects")

(defvar scad--indent-level 0
  "Current indendation level for `scad-convert'")

(defvar scad--needs-newline nil
  "Whether or not the next `scad--indented-print' needs a newline first")

(defun scad--inc-indent ()
  (cl-incf scad--indent-level)
  (setq scad--needs-newline nil)
  "")

(defun scad--dec-indent ()
  (cl-decf scad--indent-level)
  (setq scad--needs-newline nil)
  "")

(defun scad--indentation ()
  (cl-loop repeat scad--indent-level
           concat scad-indent-str))

(defun scad--format (str &rest values)
  (apply #'format
         (cons
          (concat
           (if scad--needs-newline "\n" "")
           (progn (setq scad--needs-newline t)
                  (scad--indentation))
           str
           "\n")
          values)))

(defun scad--round-float-str (value)
  (format "%s"
          (if (<= 0 scad-max-precision)
              (/ (fround (* value (expt 10.0 scad-max-precision)))
                 (expt 10.0 scad-max-precision))
            value)))

(defun scad--number->string (value)
  (if (<= 0 scad-max-precision)
      (format (format "%%.%df" scad-max-precision) value)
    (format "%f" value)))

(defun scad--list->scad-vector (data)
  (format "[%s]" (mapconcat #'scad--number->string data ", ")))

(defun scad--list->scad-array-2d (data)
  (format "[%s]" (mapconcat #'scad--list->scad-vector data ", ")))

(defun scad--var-if (value var)
  (if value
      (format ", %s = %s" var value)
    ""))

(defun scad--var-if-str (value var)
  (if value
      (format ", %s = \"%s\"" var value)
    ""))

(defun scad--var-if-vector (value var)
  (if value
      (format ", %s = %s" var (scad--list->scad-vector value))
    ""))

(defun scad--val->scad-vector-or-val (value)
  (cond
   ((listp value)
    (scad--list->scad-vector value))
   ((stringp value)
    (format "\"%s\"" value))
   (t
    (scad--number->string value))))

(defun scad-set-variable (var value)
  (scad--format "%s = %s;" var value))


;;; Boolean operations
(defmacro scad-union (&rest body)
  `(concat
    (scad--format "union() {")
    (scad--inc-indent)
    ,@body
    (scad--dec-indent)
    (scad--format "}  // union")))

(defmacro scad-difference (&rest body)
  "SCAD 'difference'. The rest of the body is subtracted from the first element."
  (declare (indent 1))
  `(concat
    (scad--format "difference() {")
    (scad--inc-indent)
    ,@body
    (scad--dec-indent)
    (scad--format "}  // difference")))

(defmacro scad-intersection (&rest body)
  `(concat
    (scad--format "intersection() {")
    (scad--inc-indent)
    ,@body
    (scad--dec-indent)
    (scad--format "}  // intersection")))

;;; Modifier characters
;; disable: Add *
;; show-only: Add !
;; highlight: Add #
;; transparent: Add %


;;; 2D
(defun scad-circle (radius &optional fragment-angle fragment-size num-fragments)
  (scad--format "circle(%s%s%s%s);"
                (scad--number->string radius)
                (scad--var-if fragment-angle "$fa")
                (scad--var-if fragment-size "$fs")
                (scad--var-if num-fragments "$fn")))

(defun scad-square (size &optional center)
  (scad--format "square(%s%s);"
                (scad--val->scad-vector-or-val size)
                (if center ", center = true" "")))

(defun scad-polygon (points &optional paths convexity)
  (scad--format "polygon(%s%s%s);"
                (scad--list->scad-array-2d points)
                (if paths
                    (concat ", " (scad--list->scad-array-2d paths))
                  "")
                (scad--var-if convexity "convexity")))

(defun scad-relative-polygon (start-x start-y points)
  "Call `scad-polygon' with a list of points that are relative to
the previous point. The `start-x' and `start-y' define the first
point."
  (declare (indent 2))
  (scad-polygon
   (append `((,start-x ,start-y))
           (cl-loop with current-x = start-x
                    with current-y = start-y
                    for point in points
                    do (progn (setq current-x (+ current-x (car point)))
                              (setq current-y (+ current-y (cadr point))))
                    collect (list current-x current-y)))))

(defun scad-regular-polygon (num-sides radius &optional type rotated angle)
  "Create a regular polygon of `num-sides' with the first point towards +x. Keywords:
`type': `:outer' (default) or `:inner' for radius
`rotated': Initial rotation to point up (default nil)
`angle': Counter-clockwise rotation angle, in degrees (default 0)"
  (let ((radius (if (eq :outer (if type type :outer))
                    (* 1.0 radius)
                  (/ radius (cos (/ pi num-sides)))))
        (angle (* (+ (if angle angle 0.0) (if rotated 90.0 0.0)) (/ pi 180))))
    (scad-polygon (cl-loop for ii below num-sides
                           collect (let ((point-angle (+ angle (* ii 2 pi (/ 1.0 num-sides)))))
                                     (list (* radius (cos point-angle))
                                           (* radius (sin point-angle))))))))

(defun scad-star (num-points inner-radius outer-radius &optional rotated angle)
  "Create a star `num-points' with the first point towards +x. Keywords:
`rotated': Initial rotation to point up (default nil)
`angle': Counter-clockwise rotation angle, in degrees (default 0)"
  (let ((angle (* (+ (if angle angle 0.0) (if rotated 90.0 0.0)) (/ pi 180))))
    (scad-polygon (cl-loop for ii below num-points
                           collect (let ((point-angle (+ angle (* ii 2 pi (/ 1.0 num-points)))))
                                     (list (* outer-radius (cos point-angle))
                                           (* outer-radius (sin point-angle))))
                           collect (let ((inner-angle (+ angle (* (+ 0.5 ii) 2 pi (/ 1.0 num-points)))))
                                     (list (* inner-radius (cos inner-angle))
                                           (* inner-radius (sin inner-angle))))))))

(defun scad-text (text
                  &optional size font h-align v-align spacing direction language script num-fragments)
  (scad--format "text(\"%s\", size = %s%s%s%s%s%s%s%s%s);"
                text
                (scad--number->string (if size size 10))
                (scad--var-if-str font "font")
                (scad--var-if-str h-align "halign")
                (scad--var-if-str v-align "valign")
                (scad--var-if spacing "spacing")
                (scad--var-if-str direction "direction")
                (scad--var-if-str language "language")
                (scad--var-if-str script "script")
                (scad--var-if num-fragments "$fn")))

(defun scad-import-file (name &optional convexity layer num-fragments fragment-angle fragment-size)
  (scad--format "import(%s%s%s%s%s%s);"
                name
                (scad--var-if convexity "convexity")
                (scad--var-if layer "layer")
                (scad--var-if num-fragments "$fn")
                (scad--var-if fragment-angle "$fa")
                (scad--var-if fragment-size "$fs")))

(defun scad-projection (&optional cut)
  (scad--format (if cut "projection(cut = true);" "projection();")))


;;; 3D
(defun scad-sphere (radius &optional fragment-angle fragment-size num-fragments)
  (scad--format "sphere(%s%s%s%s);"
                (scad--number->string radius)
                (scad--var-if fragment-angle "$fa")
                (scad--var-if fragment-size "$fs")
                (scad--var-if num-fragments "$fn")))

(defun scad-cube (x y z &optional center)
  (scad--format "cube(%s%s);"
                (scad--list->scad-vector (list x y z))
                (if center ", center = true" "")))

(defun scad-cylinder (height radius &optional r2 center fragment-angle fragment-size num-fragments)
  (scad--format "cylinder(h = %s, r1 = %s, r2 = %s%s%s%s%s);"
                (scad--number->string height)
                (scad--number->string radius)
                (if r2
                    (scad--number->string r2)
                  (scad--number->string radius))
                (if center ", center = true" "")
                (scad--var-if fragment-angle "$fa")
                (scad--var-if fragment-size "$fs")
                (scad--var-if num-fragments "$fn")))

(defun scad-cone (height radius &optional center fragment-angle fragment-size num-fragments)
  "Wrapper for `scad-cylinder' to produce a cone"
  (scad-cylinder height radius 0 center fragment-angle fragment-size num-fragments))

(defun scad-polyhedron (points faces &optional convexity)
  (scad--format "polyhedron(points = %s, faces = %s%s);"
                (scad--list->scad-array-2d points)
                (scad--list->scad-array-2d faces)
                (scad--var-if convexity "convexity")))

(cl-defmacro scad-linear-extrude
    ((height &optional center convexity twist slices scale num-fragments) &rest body)
  "extrude"
  (declare (indent 1))
  `(concat
    (scad--format "linear_extrude(height = %s%s%s%s%s%s%s) {"
                  (scad--number->string ,height)
                  (if ,center ", center = true" "")
                  (scad--var-if ,convexity "convexity")
                  (scad--var-if ,twist "twist")
                  (scad--var-if ,slices "slices")
                  (scad--var-if ,scale "scale")
                  (scad--var-if ,num-fragments "$fn"))
    (scad--inc-indent)
    ,@body
    (scad--dec-indent)
    (scad--format "}  // linear_extrude")))

(cl-defmacro scad-rotate-extrude
    ((angle &optional convexity fragment-angle fragment-size num-fragments) &rest body)
  "SCAD 'extrude' command"
  (declare (indent 1))
  `(concat
    (scad--format "rotate_extrude(angle = %s%s%s%s%s) {"
                  (scad--number->string ,angle)
                  (scad--var-if ,convexity "convexity")
                  (scad--var-if ,fragment-angle "$fa")
                  (scad--var-if ,fragment-size "$fs")
                  (scad--var-if ,num-fragments "$fn"))
    (scad--inc-indent)
    ,@body
    (scad--dec-indent)
    (scad--format "}  // rotate_extrude")))

(defun scad-surface (name &optional center invert convexity)
  (scad--format "surface(file = %s%s%s%s);"
                name
                (if center ", center = true" "")
                (if invert ", invert = true" "")
                (scad--var-if convexity "convexity")))


;;; Transformations
(cl-defmacro scad-translate (x y z &rest body)
  "SCAD 'translate'"
  (declare (indent 3))
  `(concat
    (scad--format "translate(%s) {"
                  (scad--list->scad-vector (list ,x ,y ,z)))
    (scad--inc-indent)
    ,@body
    (scad--dec-indent)
    (scad--format "}  // translate")))
;; jpm here
(cl-defmacro scad-rotate ((angle &optional vector) &rest body)
  "SCAD 'rotate'"
  (declare (indent 1))
  `(concat
     (scad--format "rotate(a = %s%s) {"
                   (scad--val->scad-vector-or-val ,angle)
                   (scad--var-if-vector ,vector "v"))
     (scad--inc-indent)
     ,@body
     (scad--dec-indent)
     (scad--format "}  // rotate")))

(cl-defmacro scad-scale (x y z &rest body)
  "SCAD 'scale'"
  (declare (indent 3))
  `(concat
     ;; Don't change precision on scale
     (scad--format "scale([%f, %f, %f]) {" ,x ,y ,z)
     (scad--inc-indent)
     ,@body
     (scad--dec-indent)
     (scad--format "}  // scale")))

(cl-defmacro scad-resize ((x y z &optional auto) &rest body)
  "SCAD 'resize'"
  (declare (indent 1))
  `(concat
     (scad--format "resize([%s, %s, %s]%s) {"
                   (scad--round-float-str ,x)
                   (scad--round-float-str ,y)
                   (scad--round-float-str ,z)
                   (if ,auto ", auto = true" ""))
     (scad--inc-indent)
     ,@body
     (scad--dec-indent)
     (scad--format "}  // resize")))

(cl-defmacro scad-mirror (x y z &rest body)
  "SCAD 'mirror'"
  (declare (indent 3))
  `(concat
     ;; Don't change precision on mirror
     (scad--format "mirror([%f, %f, %f]) {" ,x ,y ,z)
     (scad--inc-indent)
     ,@body
     (scad--dec-indent)
     (scad--format "}  // mirror")))

(cl-defmacro scad-multmatrix (matrix &rest body)
  "SCAD 'multmatrix'"
  (declare (indent 1))
  `(concat
     (scad--format "multmatrix(%s) {" (scad--list->scad-array-2d ,matrix))
     (scad--inc-indent)
     ,@body
     (scad--dec-indent)
     (scad--format "}  // multmatrix")))

(cl-defmacro scad-color ((color &optional alpha) &rest body)
  "SCAD 'color'"
  (declare (indent 1))
  `(concat
    (scad--format "color(%s%s) {"
                  (scad--val->scad-vector-or-val ,color)
                  (scad--var-if ,alpha "alpha"))
    (scad--inc-indent)
    ,@body
    (scad--dec-indent)
    (scad--format "}  // color")))

(cl-defmacro scad-radius-offset ((radius) &rest body)
  "SCAD 'offset' using `radius'"
  (declare (indent 1))
  `(concat
    (scad--format "offset(r = %s) {" (scad--round-float-str ,radius))
    (scad--inc-indent)
    ,@body
    (scad--dec-indent)
    (scad--format "}  // offset(radius)")))

(cl-defmacro scad-linear-offset ((delta &optional chamfer) &rest body)
  "SCAD 'offset' using `delta' (linear)"
  (declare (indent 1))
  `(concat
    (scad--format "offset(delta = %s%s) {"
                  (scad--round-float-str ,delta)
                  (if ,chamfer ", chamfer = true" ""))
    (scad--inc-indent)
    ,@body
    (scad--dec-indent)
    (scad--format "}  // offset(linear)")))

(cl-defmacro scad-fillet (radius &rest body)
  "SCAD 'offset' to make a fillet"
  (declare (indent 1))
  `(concat
    (scad--format "offset(r = -%s) offset(delta = %s) {"
                  (scad--round-float-str ,radius) (scad--round-float-str ,radius))
    (scad--inc-indent)
    ,@body
    (scad--dec-indent)
    (scad--format "}  // offset(fillet)")))

(cl-defmacro scad-round-outline (radius &rest body)
  "SCAD 'offset' using rounded corners"
  (declare (indent 1))
  `(concat
    (scad--format "offset(r = %s) offset(delta = -%s) {"
                  (scad--round-float-str ,radius) (scad--round-float-str ,radius))
    (scad--inc-indent)
    ,@body
    (scad--dec-indent)
    (scad--format "}  // offset(round)")))

(cl-defmacro scad-hull (&rest body)
  `(concat
     (scad--format "hull() {")
     (scad--inc-indent)
     ,@body
     (scad--dec-indent)
     (scad--format "}  // hull")))

(cl-defmacro scad-minkowski (&rest body)
  `(concat
     (scad--format "minkowski() {")
     (scad--inc-indent)
     ,@body
     (scad--dec-indent)
     (scad--format "}  // minkowski")))

;; Other

(defun scad-echo (message)
  "SCAD 'echo' command"
  (scad--format "echo(%s);" message))

(cl-defmacro scad-render (convexity &rest body)
  "SCAD 'render' command"
  (declare (indent 1))
  `(concat
    (scad--format "render(convexity = %s) {" ,convexity)
    (scad--inc-indent)
    ,@body
    (scad--dec-indent)
    (scad--format "}  // render")))

;; TODO children

;; TODO assert

(defun scad-comment (message)
  "Insert a comment into the SCAD script"
  (let ((comment (scad--format "// %s" message)))
    (setq scad--needs-newline nil)
    comment))

(defmacro scad-string (&rest body)
  "Convert to a SCAD script string"
  `(progn
     (setq scad--indent-level 0)
     (setq scad--needs-newline nil)
     (concat ,@body)))

(defmacro scad-buffer (buffer-name &rest body)
  "Write out the SCAD script to `buffer-name'.
  It will create the buffer if it does not exist.
  It will delete all contents of the target buffer if it does exist."
  (declare (indent 1))
  `(progn
     (switch-to-buffer (get-buffer-create ,buffer-name))
     (delete-region (point-min) (point-max))
     (setq scad--indent-level 0)
     (setq scad--needs-newline nil)
     ,@(cl-loop for item in body
                collect `(insert ,item))
     ;; (insert ,@body)
     ))

(defmacro scad-write-file (filename &rest body)
  "Write out the SCAD script to `filename'"
  (declare (indent 1))
  `(with-temp-buffer
     (setq scad--indent-level 0)
     (setq scad--needs-newline nil)
     ,@(cl-loop for item in body
                collect `(insert ,item))
     ;; (insert ,@body)
     (write-file ,(if (eq :auto filename)
                      (concat (file-name-sans-extension (buffer-file-name)) ".scad")
                    filename))))

(defmacro scad-let (VARS &rest BODY) 
  "Macro to wrap `cl-flet' and `let*' for ease of use with other scad commands

In `VARS':
- If an element begins with `:PRE'/`:pre', it will be added to a
  `cl-flet' that is defined before the `let*' (so that these
  functions may be used when defining variables)
- If an element looks like a flet form: `(name (args) body)`, it
  will be added to an inner `cl-flet', after the `let*'
- Elements that look like a symbol name, or a list of `(name
  value)` will be added to the `let*` form
- Note: Also starts with `(fudge scad-fudge)` and `(-fudge (- fudge))` on
  the `let*' variables

Example:
(scad-let
    (a
     (b 10)
     (:PRE sq (x) (* x x))
     (c (sq 12))
     (foo (x) (message \"Foo: %s\" x)))
  (foo (list a c)))

Would expand to:
(cl-flet ((sq (x) (* x x)))
  (let* ((fudge scad-fudge)
         (-fudge (- fudge))
         a
         (b 10)
         (c (sq 12)))
    (cl-flet ((foo (x) (message \"Foo: %s\" x))))))
"
  (declare (indent 1))
  (let ((let-vars '((-fudge (- fudge)) (fudge scad-fudge)))
        funcs
        pre-funcs
        (inch-symbols '(in IN inch INCH :in :IN :inch :INCH)))
    (cl-loop for item in VARS
             do (message "%s" item)
             do (let ((item-len (if (listp item) (length item) 0)))
                  (cond
                   ((or (symbolp item)
                        (and (listp item)
                             (= 2 item-len)))
                    (setq let-vars (cons item let-vars)))
                   ((not (listp item))
                    (message "Unexpected item in VARS: %s" item))
                   ((and (= 3 item-len)
                         (memq (cl-third item) inch-symbols))
                    (setq let-vars (cons (list (car item) `(* 25.4 ,(cadr item))) let-vars)))
                   ((memq (car item) '(:PRE :pre))
                    (setq pre-funcs (cons (cdr item) pre-funcs)))
                   (t
                    (setq funcs (cons item funcs))))))
    `(cl-flet ,(reverse pre-funcs)
       (let* ,(reverse let-vars)
         (cl-flet ,(reverse funcs)
           ,@BODY)))))

(provide 'scad)
